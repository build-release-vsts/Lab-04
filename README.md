# CI/CD with Azure DevOps
Lab 04: Create your first Release Pipeline

---

## Preparation

 - Configure a second build agent in a different pool (env-pool)
 
 
## Instructions


## Create a release pipeline definition

 - Browse to "Releases" page
 
 - click on "Create a new pipeline"
 
 - Select the "Empty Job" template


## Configure artifacts

 - Click on "add a new artifact"
 - Configure a single artifact as below:

```
Source Type: Azure Artifacts
Feed: my-feed
Package Type: Universal
Package: my-artifact
Default Version: Latest
Source alias: _npm-ci
```

<img alt="Image release-01" src="images/release-01.PNG"  width="75%" height="75%">

 - Configure the artifact trigger:

```
Continuous deployment trigger: Enabled
```

<img alt="Image 1.2" src="images/task-1.2.png"  width="75%" height="75%">

## Configure the first environment (Dev)

 - Access to the environment configuration:


<img alt="Image 1.3" src="images/task-1.3.png"  width="75%" height="75%">


 - Configure the Stage name:

```
Stage Name: Dev
```

<img alt="Image 1.4" src="images/task-1.4.png"  width="75%" height="75%">


 - Configure the Agent phase:

```
Display name: Deployment
Agent queue: my-pool
```

<img alt="Image release-02" src="images/release-02.png"  width="75%" height="75%">


 - Configure task "Powershell"

```
Task: Powershell
Display name: Stop Services
Type: Inline
Script:

if((Get-Process).ProcessName.Contains("cmd")){Stop-Process -Name cmd -force}
Start-Sleep -Seconds 3
if(Test-Path "C:\npm-app"){Remove-Item -Path C:\npm-app\* -Force -Recurse}
```

<img alt="Image 1.6" src="images/task-1.6.png"  width="75%" height="75%">

- Configure task "Extract Files"

```
Display name: Extract Files
Archive file patterns: *.zip
Destination folder: "C:\npm-app"
Clean destination folder before extracting: False
```

<img alt="Image 1.7" src="images/task-1.7.png"  width="75%" height="75%">

 - Configure task "npm"

```
Display name: npm install
Command: install
Working folder with package.json: "C:\npm-app"
```

<img alt="Image 1.8" src="images/task-1.8.png"  width="75%" height="75%">

 - Configure task "powershell"

```
Display name: start
Type: Inline
Script: Start-Process -FilePath "C:\Program Files\nodejs\npm" -ArgumentList start -WorkingDirectory "C:\npm-app"
```

<img alt="Image 1.9" src="images/task-1.9.png"  width="75%" height="75%">


## Configure the second environment (Production) - Optional

 - Configure the Agent phase:

```
Display name: Deployment
Agent queue: env-pool
```

 - Configure task "Powershell"

```
Task: Powershell
Display name: Stop Services
Type: Inline
Script:

if((Get-Process).ProcessName.Contains("cmd")){Stop-Process -Name cmd}
Start-Sleep -Seconds 3
if(Test-Path "C:\npm-app"){Remove-Item -Path C:\npm-app\* -Force -Recurse}
```

<img alt="Image 1.6" src="images/task-1.6.png"  width="75%" height="75%">

- Configure task "Extract Files"

```
Display name: Extract Files
Archive file patterns: *.zip
Destination folder: "C:\npm-app"
```

<img alt="Image 1.7" src="images/task-1.7.png"  width="75%" height="75%">

 - Configure task "npm"

```
Display name: npm install
Command: custom
Working folder with package.json: "C:\npm-app"
Command and arguments: install
```

<img alt="Image 1.8" src="images/task-1.8.png"  width="75%" height="75%">

 - Configure task "powershell"

```
Display name: start
Type: Inline
Script: Start-Process -FilePath "C:\Program Files\nodejs\npm" -ArgumentList start -WorkingDirectory "C:\npm-app"
```

<img alt="Image 1.9" src="images/task-1.9.png"  width="75%" height="75%">


## Set Pre-Deployment Condition for "Production"

 - Configure the Pre-Deployment Condition:

```
Pre-deployment approvals: Enabled
Approvers: <your-user>
```

<img alt="Image 1.10" src="images/task-1.10.png"  width="75%" height="75%">
